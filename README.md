# Portable GPS Devices #

Portable GPS devices for tracking consignments, developed on ATmega328P using sim800l GSM module and three different GPS modules - Skylab, Ublox Neo6M and sim808.

1. ultimate_final.ino - Skylab Gps Module
2. gsm_gps_tracker - SIM808 Gps Module
3. gps_gsm_neo6M - Ublox Neo6M Module
